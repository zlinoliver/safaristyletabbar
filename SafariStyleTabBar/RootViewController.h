//
//  RootViewController.h
//  SafariStyleTabBar
//
//  Created by zaker-7 zaker-7 on 12-4-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SafariStyleScrollView;

@interface RootViewController : UIViewController<UIScrollViewDelegate>
{

    
}

@property (nonatomic,retain) SafariStyleScrollView *safariScrollView;
@property (nonatomic,retain) NSMutableArray *viewControllersArray;

@end
