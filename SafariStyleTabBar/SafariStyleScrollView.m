//
//  SafariStyleScrollView.m
//  SafariStyleTabBar
//
//  Created by zaker-7 zaker-7 on 12-4-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "SafariStyleScrollView.h"
#import "ViewController.h"
#import "DragImageView.h"

#define MAXDragImage 10
#define DragImageSize 64.0

@implementation SafariStyleScrollView
@synthesize dragImageArray = _dragImageArray;
@synthesize animationState = _animationState;
@synthesize blockDictionaryArray = _blockDictionaryArray;
@synthesize dragImageDataDic = _dragImageDataDic;
@synthesize movingToLeft;
@synthesize movingToRight;
@synthesize prePoint = _prePoint;
@synthesize currentPoint=_currentPoint;
@synthesize publicDragImageView = _publicDragImageView;

-(void)dealloc
{
    [super dealloc];
    self.dragImageDataDic = nil;
    self.dragImageArray = nil;
    self.blockDictionaryArray = nil;
    
    [_publicDragImageView release];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        ///////////////////0: AnimationEnd 1: AnimationStart///////////////////////////////////////
        self.animationState = 0;
        
        
        ///////////////////////////Initialize Arrays//////////////////////////////////////////////////
        self.dragImageArray = [[[NSMutableArray alloc]init] autorelease];
        self.blockDictionaryArray = [[[NSMutableArray alloc]init] autorelease];
        self.dragImageDataDic = [[[NSMutableDictionary alloc] init] autorelease];
        
        //////////////////////////Add Block Dictionary to BlockDictionaryArray/////////////////////////////////////
        
        [self initializeBlockDictionary];
        
        
        [self loadDragImageViewInView:self withDragImageNum:MAXDragImage];    
    
    
    }
    return self;
}

/**
 添加拖动图片到ScrollView中，个性化设置添加的图片数量
 @param UIScrollView 被添加到的ScrollView
 @param int 添加图片数量
 */
-(void)loadDragImageViewInView:(UIScrollView *)scrollView withDragImageNum:(int)dragImageNum
{

    
    for (int i = 0; i < dragImageNum; i++) {
        
        DragImageView *dragImage = [[DragImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"blueFlower_00%d.png",i]]];
        dragImage.userInteractionEnabled = YES;
        dragImage.imageId = i;
        dragImage.relatedViewControllerId = i;
        dragImage.tag = 100 +i;
        
        ///////////////////////////////////////Update DragImageView's Position////////////////////////////////////////
        if ([[NSUserDefaults standardUserDefaults] valueForKey:@"DragImageDataDic"]) {
            
            NSData *dragImageDicData = [[NSUserDefaults standardUserDefaults] valueForKey:@"DragImageDataDic"]; 
            NSMutableDictionary *dragImageDataDictionary = [[NSKeyedUnarchiver unarchiveObjectWithData:dragImageDicData] mutableCopy];
            
            dragImage.center = CGPointMake([[dragImageDataDictionary objectForKey:[NSString stringWithFormat:@"%dXPostion",dragImage.imageId]] floatValue],  DragImageSize/2);
            
            [dragImageDataDictionary release];
            
            dragImage.imageViewPosition = dragImage.center;
            
        }else {
            
            dragImage.center = CGPointMake(DragImageSize/2 + DragImageSize * i, DragImageSize/2);
            dragImage.imageViewPosition = dragImage.center;
        }
        
        [self.dragImageArray addObject:dragImage];
        [self.dragImageDataDic setObject : [NSString stringWithFormat:@"%f",dragImage.imageViewPosition.x] forKey:[NSString stringWithFormat:@"%dXPostion",dragImage.imageId]];
        
        /////////////////////////Add LongPressGestureRecognizer///////////////////////////////////////    
        
        UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(DragEnable:)];
        lpgr.minimumPressDuration = 0.2;
        lpgr.delegate = self;
        [dragImage addGestureRecognizer:lpgr];
        [lpgr release];
        
        ////////////////////////Add TapGestureRecognizer/////////////////////////////////////////////
        UITapGestureRecognizer *tgr = [[UITapGestureRecognizer  alloc]initWithTarget:self action:@selector(tapHandler:)];
        tgr.numberOfTapsRequired = 2;
        tgr.numberOfTouchesRequired = 1;
        [dragImage addGestureRecognizer:tgr];
        [tgr release];
        
        
        [scrollView addSubview:dragImage];
        [dragImage release];
        
    }
    
}

/**
 初始化动画触发区间数据字典
 */
-(void)initializeBlockDictionary
{
    float startPoint,endPoint,midPoint;
    
    for (int i = 0; i < 10; i++) {
        
        startPoint = (0.75 + i) * DragImageSize;
        endPoint = (1.25 + i) * DragImageSize;
        midPoint = (0.5 + i) * DragImageSize;
        
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%f",startPoint],@"StartPoint",[NSString stringWithFormat:@"%f",midPoint],@"MidPoint", [NSString stringWithFormat:@"%f",endPoint ], @"EndPoint",nil];
        [self.blockDictionaryArray addObject:dictionary];
    }


}

/**
 双击击图片操作成功后触发的方法
 @param UIGestureRecognizer 双击操作对应的UIGestureRecognizer变量
 */
- (void) tapHandler: (UITapGestureRecognizer *)gestureRecognizer{
    DragImageView *dragImageView = (DragImageView *)gestureRecognizer.view;
    [dragImageView setImage:[UIImage imageNamed:@"orangeFlower.png"]];
    
}

/**
 移动右侧图片到左侧的恰当位置
 @param float 被移动的右侧图片坐标位置
 @param DragImageView 当前拖动的DragImageView图片对象
 */
-(void)moveRightImageViewToLeftWithLocation:(float)rightLocation andDraggingImageView:(DragImageView *)dragImageView;

{
    
    self.animationState = 0;
    
    for (DragImageView *singleDragImageView in self.dragImageArray) {
        
        
        if (singleDragImageView.center.x == rightLocation) {
            
            if (self.animationState == 0) {
                
                /////////////////////Animate The movement/////////////////////////////////
                
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.3];
                [UIView setAnimationDelay:0.0];
                [UIView setAnimationDelegate:self];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                
                singleDragImageView.center = CGPointMake(singleDragImageView.center.x - 64, singleDragImageView.center.y);
                
                /////////////////////////////////////Update DragImageDataDictionary////////////////////////////////////////
                [self.dragImageDataDic setObject:[NSString stringWithFormat:@"%f", singleDragImageView.center.x] forKey:[NSString stringWithFormat:@"%dXPostion",singleDragImageView.imageId]];
                
                [UIView commitAnimations];
                
                
                self.animationState = 1;
                
            }else if (self.animationState == 1)
            {
                
                
            }
            
        }
    }
    
    
}

/**
 移动左侧图片到右侧的恰当位置
 @param float 被移动的左侧图片坐标位置
 @param DragImageView 当前拖动的DragImageView图片对象
 */
-(void)moveLeftImageViewToRightWithLocation:(float)leftLocation andDraggingImageView:(DragImageView *)dragImageView;
{
    
    self.animationState = 0;
    
    for (DragImageView *singleDragImageView in self.dragImageArray) {
        
        if (singleDragImageView.center.x == leftLocation) {
            
            
            if (self.animationState == 0) {
                
                ////////////Animate The movement/////////////////////////////////
                
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:0.5];
                [UIView setAnimationDelay:0.0];
                [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
                //    [UIView setAnimationDidStopSelector:@selector(UIViewAnimationsDidStop:finished:context:)];
                
                singleDragImageView.center = CGPointMake(singleDragImageView.center.x + 64, singleDragImageView.center.y);
                
                /////////////////////////////////////Update DragImageDataDictionary////////////////////////////////////////
                [self.dragImageDataDic setObject:[NSString stringWithFormat:@"%f", singleDragImageView.center.x] forKey:[NSString stringWithFormat:@"%dXPostion",singleDragImageView.imageId]];
                
                [UIView commitAnimations];
                
                
                self.animationState = 1;
                
            }else if (self.animationState == 1)
            {
                
                
            }
            
        }
    }
    
    
}

/**
 根据动画触发区间数据字典和当前拖动的DragImageView图片对象，播放移动动画
 @param NSDictionary 动画触发区间数据字典
 @param DragImageView 当前拖动的DragImageView图片对象
 */
-(void)playAnimationByCheckingMovingDirectionWithDic:(NSDictionary *)dic andImageView:(DragImageView *)dragImageView  
{
    if (self.movingToLeft) {
        
        [self moveLeftImageViewToRightWithLocation:[[dic objectForKey:@"MidPoint"]floatValue] andDraggingImageView:dragImageView];
    }
    
    if (self.movingToRight) {
        
        [self moveRightImageViewToLeftWithLocation:([[dic objectForKey:@"MidPoint"]floatValue] + DragImageSize) andDraggingImageView:dragImageView];
    }
    
    
}

/**
 获取当前拖动所在图片区域
 @param float 当前拖动图片的X坐标
 @returns 返回所在图片区域
 */
-(int)getCurrentZoomWithXPosition:(float)XPosition
{
    
    if (XPosition >= 1.0 && XPosition <= DragImageSize ) {
        return 1;
    }else if (XPosition > DragImageSize && XPosition <= DragImageSize*2)
    {
        return 2;
    }else if (XPosition > DragImageSize*2 && XPosition <= DragImageSize*3)
    {
        return 3;
    }else if(XPosition > DragImageSize*3 && XPosition <= DragImageSize*4)
    {
        return 4;
    }else if(XPosition > DragImageSize*4 && XPosition <= DragImageSize*5)
    {
        return 5;
    }else if(XPosition > DragImageSize*5 && XPosition <= DragImageSize*6)
    {
        return 6;
    }else if(XPosition > DragImageSize*6 && XPosition <= DragImageSize*7)
    {
        return 7;
    }else if(XPosition > DragImageSize*7 && XPosition <= DragImageSize*8)
    {
        return 8;
    }else if(XPosition > DragImageSize*8 && XPosition <= DragImageSize*9)
    {
        return 9;
    }else if(XPosition > DragImageSize*9 && XPosition <= DragImageSize*10)
    {
        return 10;
    }
    
    return 0;
    
}


/**
 根据当前所在图片区域和当前拖动图片坐标位置，判断是否进入动画触发区间的右半区域
 @param int 当前所在图片区域
 @param float 当前拖动图片坐标位置
 @returns 返回布尔值，判断是否进入区域
 */

-(BOOL)IfCrossOverEndPointWithZoom:(int)currentZoom andPosition:(float)XPosition
{
    
    if (!(currentZoom == 1 || currentZoom == 6)) {// Check special zoom in case some bugs happen
        
        if (XPosition < [[[self.blockDictionaryArray objectAtIndex:currentZoom-2] objectForKey:@"EndPoint"] floatValue] ) {
            return YES;   
        }else {
            return NO;
        }
    }
    
    return NO;
    
}

/**
 根据当前所在图片区域和当前拖动图片坐标位置，判断是否进入动画触发区间的左半区域
 @param int 当前所在图片区域
 @param float 当前拖动图片坐标位置
 @returns 返回布尔值，判断是否进入区域
 */
-(BOOL)IfCrossOverStartPointWithZoom:(int)currentZoom andPosition:(float)XPosition
{
    
    if (!(currentZoom == MAXDragImage || currentZoom == 5)) {// Check special zoom in case some bugs happen
        
        if (XPosition > [[[self.blockDictionaryArray objectAtIndex:currentZoom-1] objectForKey:@"StartPoint"] floatValue]) {
            
            return YES;   
            
        }else {
            
            return NO;
        }
    }
    
    return NO;
    
    
}


/**
 根据当前拖动的DragImageView图片对象和状态机参数，移动到特定的图片区域
 @param int 当前所在区域
 @param DragImageView 当前拖动的DragImageView图片对象
 @param int 状态机状态: 1: MoveLeft, 2: MoveRight, 0: Reset
 */
-(void)moveToZoomCenterWithZoom:(int)zoom andDragImageView:(DragImageView *)dragImageView andState:(int)state
{
    
    switch (state) {
        case 0:
            
            dragImageView.center = CGPointMake(DragImageSize* (zoom - 0.5), dragImageView.center.y);
            
            break;
        case 1:
            
            
            dragImageView.center = CGPointMake(DragImageSize* (zoom-1.5), dragImageView.center.y);
            
            
            break;
            
        case 2:
            
            dragImageView.center = CGPointMake(DragImageSize* (zoom + 0.5), dragImageView.center.y);
            
            break;
            
        default:
            break;
    }
    
    
    
}

/**
 长按图片操作成功后触发的方法
 @param UIGestureRecognizer 长按操作对应的UIGestureRecognizer变量
 */
-(void)DragEnable:(UIGestureRecognizer *)gestureRecognizer
{
    if([gestureRecognizer state] ==  UIGestureRecognizerStateBegan)
    {
        
        ////////////////////Update PrePoint data////////////////////////////////////////
        _prePoint = [gestureRecognizer locationInView:self];
    }
    
    else if(gestureRecognizer.state == UIGestureRecognizerStateChanged)
    {
        
        
        CGPoint _pan_endPoint= [gestureRecognizer locationInView:self];
        
        ////////////////////Update CurrentPoint data/////////////////////////////////////
        _currentPoint = _pan_endPoint;
        
        /////////////////////Detect user's dragging direction////////////////
        if(_currentPoint.x > _prePoint.x)
        {
            
            self.movingToRight = YES;
            self.movingToLeft = NO;
        }
        else {
            
            self.movingToLeft = YES;
            self.movingToRight = NO;
            
        }
        
        DragImageView *dragImageView = [self.dragImageArray objectAtIndex:(gestureRecognizer.view.tag-100)];
        
        /////////////////////////Update dragImageView position////////////////////////////
        CGPoint newPosition = CGPointMake( _pan_endPoint.x, dragImageView.center.y); 
        
        dragImageView.center = newPosition;
        
        for (NSDictionary *dic in self.blockDictionaryArray) {
            
            if (dragImageView.center.x > [[dic objectForKey:@"StartPoint"] floatValue] && dragImageView.center.x < [[dic objectForKey:@"EndPoint"]floatValue]) {
                
                int dragImageIndex =[self.blockDictionaryArray indexOfObject:dic];
                
                if(dragImageIndex >=0 && dragImageIndex < 9)
                {
                    if (!(dragImageIndex == 4 || dragImageIndex == 9)) {
                        
                        [self playAnimationByCheckingMovingDirectionWithDic:dic andImageView:dragImageView];
                    }
                    
                }
                
            }
            
            
        }        
        
        ////////////////////////Update PrePoint data/////////////////////////////
        _prePoint = _pan_endPoint;
        
        
    }
    
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        
        
        //clear point record
        _prePoint = CGPointZero;
        _currentPoint = CGPointZero;
        
        /////////////////Reset AnimationState/////////////////////////////
        self.animationState = 0;
        
        
        ////////////////Fix dragImageView Position////////////////////////////////////////////
        
        DragImageView *dragImageView = [self.dragImageArray objectAtIndex:(gestureRecognizer.view.tag-100)];
        
        ///////////////////Get Current Zoom//////////////////////////////////////////////
        int currentZoom =  [self getCurrentZoomWithXPosition:dragImageView.center.x];
        
        
        ///////////////////Fix DragImageView Position////////////////////////////////////
        if ([self IfCrossOverEndPointWithZoom:currentZoom andPosition:dragImageView.center.x]) {
            
            if (self.movingToLeft) {// Move To Left
                
                [self moveToZoomCenterWithZoom:currentZoom andDragImageView:dragImageView andState:1];
                
            }else { // Reset
                [self moveToZoomCenterWithZoom:currentZoom andDragImageView:dragImageView andState:0];
            }
        }
        
        else if([self IfCrossOverStartPointWithZoom:currentZoom andPosition:dragImageView.center.x])
        {
            if (self.movingToRight) { //Move To Right
                [self moveToZoomCenterWithZoom:currentZoom andDragImageView:dragImageView andState:2];
            }
            else {// Reset
                [self moveToZoomCenterWithZoom:currentZoom andDragImageView:dragImageView andState:0];
                
            }
            
        }
        else { // Reset
            
            [self moveToZoomCenterWithZoom:currentZoom andDragImageView:dragImageView andState:0];
        }
        
        
        dragImageView.imageViewPosition = dragImageView.center;
        
        /////////////////////////////////////Update DragImageDataDictionary////////////////////////////////////////
        [self.dragImageDataDic setObject:[NSString stringWithFormat:@"%f", dragImageView.imageViewPosition.x] forKey:[NSString stringWithFormat:@"%dXPostion",dragImageView.imageId]];
        
        
        ////////////////////////////////////Store latest DragImageDataDic in NSUserDefaults///////////////////////////
        [[NSUserDefaults standardUserDefaults] setValue:[NSKeyedArchiver archivedDataWithRootObject:self.dragImageDataDic] forKey:@"DragImageDataDic"];
        

        /////////////////////////////Update public DragImageView////////////////////////////////////////
        self.publicDragImageView = (DragImageView *)gestureRecognizer.view;
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"DragImageActionFinish" object:self];
        
    }
    
    
}


///////////////////////////////Implement UIGestureRecognizer Delegate Method///////////////////////////////////////////
-(BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    
    
    return YES;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/




@end
