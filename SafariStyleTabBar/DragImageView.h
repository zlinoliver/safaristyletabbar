//
//  DragImageView.h
//  SafariStyleTabBar
//
//  Created by zaker-7 zaker-7 on 12-4-9.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DragImageView : UIImageView
{
    int imageId;
    int relatedViewControllerId;
    CGPoint imageViewPosition;

}

/**
	拖动图片ID
 */
@property(readwrite)int imageId;

/**
   拖动图片关联的ViewController ID
 */
@property(readwrite)int relatedViewControllerId;

/**
    拖动图片的坐标位置
 */
@property(assign)CGPoint imageViewPosition;

@end
