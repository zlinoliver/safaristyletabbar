//
//  RootViewController.m
//  SafariStyleTabBar
//
//  Created by zaker-7 zaker-7 on 12-4-6.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "RootViewController.h"
#import "ViewController.h"
#import "DragImageView.h"
#import "SafariStyleScrollView.h"

#define MAXDragImage 10
#define DragImageSize 64.0


@interface RootViewController ()

@end

@implementation RootViewController
@synthesize viewControllersArray = _viewControllersArray;
@synthesize safariScrollView = _safariScrollView;


-(void)dealloc
{
    
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [super dealloc];
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil

{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    
    ////////////////////////////Initialize MutableArray////////////////////////////////////////////////
    self.viewControllersArray = [[NSMutableArray alloc]init];
    
    ////////////////////////////Add SafariScrollView////////////////////////////////////////////////////
    self.safariScrollView = [[SafariStyleScrollView alloc]initWithFrame:CGRectMake(0, 396, 320, DragImageSize)];
    self.safariScrollView.contentSize = CGSizeMake(DragImageSize *10, DragImageSize);
    self.safariScrollView.backgroundColor = [UIColor yellowColor];
    self.safariScrollView.pagingEnabled = YES;
    self.safariScrollView.maximumZoomScale = 1.0;
    self.safariScrollView.minimumZoomScale = 1.0;
    self.safariScrollView.delegate = self;
    self.safariScrollView.showsVerticalScrollIndicator = NO;
    self.safariScrollView.showsHorizontalScrollIndicator = NO;
    [self.view addSubview:self.safariScrollView];

    //////////////////////////Add Related ViewController//////////////////////////////////////////
    
     for (int i = 0; i < MAXDragImage; i++) {
        
        ViewController *viewController = [[ViewController alloc]init];
        [viewController.view setFrame:CGRectMake(0, 0, 320, 396)];
        viewController.textLabel.text = [NSString stringWithFormat:@"ViewController%d", i+1];
        [self.view addSubview:viewController.view];
        [self.viewControllersArray addObject:viewController];
        [viewController release];
        
     }
     
     ///////////////////////////Add the First ViewController to Current View//////////////////////////////
     ViewController *viewControllerSingle = (ViewController *)[self.viewControllersArray objectAtIndex:0];
     [self.view bringSubviewToFront:viewControllerSingle.view];
    
    /////////////////////////////Add Notification observer to check if drag action finished//////////////////////////////////////////////
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(DragActionFinished:) name:@"DragImageActionFinish" object:nil];
}

-(void)DragActionFinished:(NSNotification *)notification
{
    if ([[notification name] isEqualToString:@"DragImageActionFinish"]) {

        ViewController *viewControllerSingle = (ViewController *)[self.viewControllersArray objectAtIndex:self.safariScrollView.publicDragImageView.relatedViewControllerId];

        
        [self.view bringSubviewToFront:viewControllerSingle.view];
    }


}

#pragma mark UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView;   
{

}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;   
{

}


- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate; 
{

}


- (void)viewDidUnload
{
    [super viewDidUnload];
    self.viewControllersArray = nil;
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


@end
