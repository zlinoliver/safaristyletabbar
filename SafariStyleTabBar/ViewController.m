//
//  ViewController.m
//  SafariStyleTabBar
//
//  Created by zaker-7 zaker-7 on 12-4-5.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize textLabel = _textLabel;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];

    self.textLabel = [[[UILabel alloc] initWithFrame:CGRectMake(60, 150, 200, 50)] autorelease];
    self.textLabel.textColor = [UIColor yellowColor];
    self.textLabel.backgroundColor = [UIColor blueColor];
    self.textLabel.textAlignment = UITextAlignmentCenter;
    [self.view addSubview:self.textLabel];
 //   [self.textLabel release];
    [self.view setBackgroundColor:[UIColor blueColor]];
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.textLabel = nil;
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}


-(void)dealloc
{
    [super dealloc];
}

@end
