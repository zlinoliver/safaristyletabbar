//
//  SafariStyleScrollView.h
//  SafariStyleTabBar
//
//  Created by zaker-7 zaker-7 on 12-4-12.
//  Copyright (c) 2012年 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DragImageView;

@interface SafariStyleScrollView : UIScrollView < UIGestureRecognizerDelegate>
{
    DragImageView *_publicDragImageView;
}


/**
   存放拖动图片数组dragImageArray
 */
@property (nonatomic,retain) NSMutableArray *dragImageArray;
/**
   存放动画响应区间数组blockDictionaryArray
 */
@property (nonatomic,retain) NSMutableArray *blockDictionaryArray;
/**
   存放动画响应区间开始点和结束点字典dragImageDataDic
 */
@property (nonatomic,retain) NSMutableDictionary *dragImageDataDic;
/**
   移动动画播放状态机animationState
 */
@property (readwrite) int animationState;
/**
   向右移动判断布尔值movingToRight
 */
@property (readwrite) BOOL movingToRight;
/**
   向左移动判断布尔值movingToLeft
 */
@property (readwrite) BOOL movingToLeft;
/**
   拖动动作的前一个坐标点prePoint
 */
@property (assign) CGPoint prePoint;
/**
   拖动动作的后一个坐标点currentPoint
 */
@property (assign) CGPoint currentPoint;
/**
   当前拖动的DragImageView
 */
@property (nonatomic,retain)DragImageView *publicDragImageView;

/**
	添加拖动图片到ScrollView中，个性化设置添加的图片数量
	@param UIScrollView 被添加到的ScrollView
	@param int 添加图片数量
 */
-(void)loadDragImageViewInView:(UIScrollView *)scrollView withDragImageNum:(int)dragImageNum;

/**
	长按图片操作成功后触发的方法
	@param UIGestureRecognizer 长按操作对应的UIGestureRecognizer变量
 */
-(void)DragEnable:(UIGestureRecognizer *)gestureRecognizer;

/**
    双击击图片操作成功后触发的方法
    @param UIGestureRecognizer 双击操作对应的UIGestureRecognizer变量
 */
- (void)tapHandler: (UITapGestureRecognizer *)gestureRecognizer;

/**
	移动右侧图片到左侧的恰当位置
	@param float 被移动的右侧图片坐标位置
	@param DragImageView 当前拖动的DragImageView图片对象
 */
-(void)moveRightImageViewToLeftWithLocation:(float)rightLocation andDraggingImageView:(DragImageView *)dragImageView;

/**
   移动左侧图片到右侧的恰当位置
   @param float 被移动的左侧图片坐标位置
   @param DragImageView 当前拖动的DragImageView图片对象
 */
-(void)moveLeftImageViewToRightWithLocation:(float)leftLocation andDraggingImageView:(DragImageView *)dragImageView;

/**
	根据动画触发区间数据字典和当前拖动的DragImageView图片对象，播放移动动画
	@param NSDictionary 动画触发区间数据字典
	@param DragImageView 当前拖动的DragImageView图片对象
 */
-(void)playAnimationByCheckingMovingDirectionWithDic:(NSDictionary *)dic andImageView:(DragImageView *)dragImageView;

/**
	初始化动画触发区间数据字典
 */
-(void)initializeBlockDictionary;


/**
	获取当前拖动所在图片区域
	@param float 当前拖动图片的X坐标
	@returns 返回所在图片区域
 */

-(int)getCurrentZoomWithXPosition:(float)XPosition;

/**
	根据当前所在图片区域和当前拖动图片坐标位置，判断是否进入动画触发区间的右半区域
	@param int 当前所在图片区域
	@param float 当前拖动图片坐标位置
	@returns 返回布尔值，判断是否进入区域
 */
-(BOOL)IfCrossOverEndPointWithZoom:(int)currentZoom andPosition:(float)XPosition;

/**
   根据当前所在图片区域和当前拖动图片坐标位置，判断是否进入动画触发区间的左半区域
   @param int 当前所在图片区域
   @param float 当前拖动图片坐标位置
   @returns 返回布尔值，判断是否进入区域
 */
-(BOOL)IfCrossOverStartPointWithZoom:(int)currentZoom andPosition:(float)XPosition;

//////////////////////State ( 1:MoveLeft, 2:MoveRight, 0: Reset)/////////////////////////////////////////////////

/**
	根据当前拖动的DragImageView图片对象和状态机参数，移动到特定的图片区域
	@param int 当前所在区域
	@param DragImageView 当前拖动的DragImageView图片对象
	@param int 状态机状态: 1: MoveLeft, 2: MoveRight, 0: Reset
 */
-(void)moveToZoomCenterWithZoom:(int)zoom andDragImageView:(DragImageView *)dragImageView andState:(int)state;



@end
